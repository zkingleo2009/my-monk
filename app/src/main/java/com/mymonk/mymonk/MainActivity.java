package com.mymonk.mymonk;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void onClick(View view) {

        if (i == 0) {
            ((ImageView) findViewById(R.id.monk)).setImageResource(R.drawable.monk_eye_open1);
            i++;
        } else if (i == 1) {
            ((ImageView) findViewById(R.id.monk)).setImageResource(R.drawable.monk_eye_open2);
            i++;
        } else if (i == 2) {
            ((ImageView) findViewById(R.id.monk)).setImageResource(R.drawable.monk_eye_closed);
            i = 0;
        }
    }
}
